using System;

class MainClass 
{
  public class Salarios
  {
    public string[] nombres;
    public float[] salario;
    public void Cargar()
    {
      nombres= new string[5];
      salario= new float[5];
      for (int i=0; i<5; i++)
      {
        Console.Write("Ingrese el nombre del empleado: ");
        nombres[i]= Console.ReadLine();
        Console.Write("Ingrese su salario: ");
        salario[i]= float.Parse(Console.ReadLine());
      }
    }
    public void SalarioMayor()
    {
      float mayor;
      int n=0;
      mayor= salario[0];
      for (int i=0; i<5;i++)
      {
        if (salario[i]>mayor)
        {
          mayor=salario[i];
          n=i;
        }
      }
      Console.WriteLine("El empleado con mayor salario es: " +nombres[n]);
      Console.WriteLine("Tiene un salario de RD${0} mensual" ,mayor);
      Console.ReadKey();
    
    }
  }
  public static void Main (string[] args) 
  {
    Salarios s= new Salarios();
    s.Cargar();
    s.SalarioMayor();
  }
}